package ar.edu.unnoba.electronicaDigital.BeaconDuino;

/**
 * Objeto utilizado para guardar los datos de los dispositivos encontrados que
 * se muestran en la lista.
 */
public class BeaconObject {
	private String name;
	private String address;
	private short rssi;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public short getRssi() {
		return rssi;
	}

	public void setRssi(short rssi) {
		this.rssi = rssi;
	}

	public double getSignalPercentage() {
		double porcentaje;
		int MIN_RSSI;
		int MAX_RSSI;

		if (this.getRssi() > 0) {
			// Para dispositivos que devuelven rssi positivo.
			// Rango calculado empiricamente.
			MIN_RSSI = 170;
			MAX_RSSI = 215;
		} else {
			// Si retorna en -dbm, este es el intervalo.
			// Rango usado por Android para WiFi.
			// Podemos usar el mismo para bluetooth.
			MIN_RSSI = -100;
			MAX_RSSI = -55;
		}

		int range = MAX_RSSI - MIN_RSSI;
		porcentaje = 100 - ((MAX_RSSI - this.getRssi()) * 100 / range);

		return porcentaje;
	}

	@Override
	public String toString() {
		return "BeaconObject [name=" + name + ", address=" + address
				+ ", rssi=" + rssi + "]";
	}
}