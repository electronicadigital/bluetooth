package ar.edu.unnoba.electronicaDigital.BeaconDuino;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.util.Log;

@SuppressLint("SimpleDateFormat")
class BeaconDuinoTools {
	private static final String TAG = "BeaconDuinoTools";
	private static final String LINE_SEPARATOR = "\n";
	private static final String ITEM_SEPARATOR = ",";

	private static final String FILE_NAME = "Lectura_Almacigos_";
	private static final String FILE_EXT = ".csv";

	public static final int FILE_WRITED = 0;
	public static final int FILE_EXCEPTION = 1;
	public static final int NO_MEDIA = 2;

	public static String variablesNames;
	public static String valuesForFile;
	public static String unitsForFile;
	public static String dateForFile;

	static public byte[] append(byte[] a, byte[] b) {
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			outputStream.write(a);
			outputStream.write(b);
			return outputStream.toByteArray();
		} catch (Exception e) {
			return null;
		}
	}

	static public String processData(String data, String signalPower,
			String rssi) {
		// Separacion de los distintos campos de los datos recibidos
		// Llegan como datos separados por comas, tanto los valores leidos
		// como los nombres de las variables y tambien las unidades.
		// Esto nos permite dejar el codigo de Android sin tocar, y modificar
		// solo el codigo del Arduino para mostrar informacion de los
		// sensores que se requieran.
		StringTokenizer st = new StringTokenizer(data, LINE_SEPARATOR);
		String variables = st.nextToken();
		variablesNames = variables;
		String values = st.nextToken();
		valuesForFile = values;
		String units = st.nextToken();
		unitsForFile = units;
		StringTokenizer varTok = new StringTokenizer(variables, ITEM_SEPARATOR);
		StringTokenizer valTok = new StringTokenizer(values, ITEM_SEPARATOR);
		StringTokenizer unitsTok = new StringTokenizer(units, ITEM_SEPARATOR);
		String sensorsReadings = "";

		// Obtenemos la fecha y la hora, y los anexamos al String.
		Date currentDate = new Date();

		// Android recomienda:
		// DateFormat sdf = SimpleDateFormat.getDateInstance()
		// DateFormat sdf = SimpleDateFormat.getTimeInstance()
		// Pero ese formato depende del usuario y el celular.
		// La siguiente implementacion es mas "estandar"
		sensorsReadings += "Potencia %: " + signalPower + "%" + LINE_SEPARATOR;
		sensorsReadings += "Potencia rssi: " + rssi + LINE_SEPARATOR;
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sensorsReadings += "Fecha: " + sdf.format(currentDate) + LINE_SEPARATOR;
		dateForFile = sdf.format(currentDate);
		sdf = new SimpleDateFormat("HH:mm:ss");
		sensorsReadings += "Hora: " + sdf.format(currentDate) + LINE_SEPARATOR;
		while (varTok.hasMoreTokens()) {
			sensorsReadings += varTok.nextToken() + ": " + valTok.nextToken()
					+ unitsTok.nextToken() + LINE_SEPARATOR;
		}
		return sensorsReadings;
	}

	/**
	 * Se obtiene el archivo con los datos que se van guardando de las lecturas
	 * de cada almacigo.
	 */
	static public File getFile() {
		DateFormat sdf = new SimpleDateFormat("dd_MM_yyyy");
		return new File(Environment.getExternalStorageDirectory(), FILE_NAME
				+ sdf.format(new Date()) + FILE_EXT);
	}

	static public String getFileName() {
		return getFile().getName();
	}

	/**
	 * Se leen los datos del archivo con las lecturas guardadas.
	 */
	static public String readDataFile() {
		// Se obtiene el archivo a leer.
		File file = getFile();
		if (file.exists()) {

			StringBuilder text = new StringBuilder();

			try {
				String line;
				BufferedReader br = new BufferedReader(new FileReader(file));
				while ((line = br.readLine()) != null) {
					text.append(line);
					text.append('\n');
				}
				br.close();
			} catch (IOException e) {
			}
			return text.toString();
		} else {
			return null;
		}
	}

	/**
	 * Se guardan los datos leidos en el archivo .csv
	 */
	static public int saveDataToFile(Context context, String name,
			String signalPower, String rssi) {
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			String varsNames = "Almacigo,Potencia %,Potencia rssi,Fecha,";
			varsNames += variablesNames;

			String values = name + "," + signalPower + "%,"+ rssi + "," + dateForFile + ",";
			values += valuesForFile;
			try {
				Log.e(TAG, Environment.getExternalStorageState());
				File root = getFile();

				FileWriter fw;
				BufferedWriter bw;
				if (!root.exists()) {
					root.createNewFile();
					fw = new FileWriter(root.getAbsoluteFile());
					bw = new BufferedWriter(fw);
					bw.append(varsNames);
				} else {
					fw = new FileWriter(root.getAbsoluteFile(), true);
					bw = new BufferedWriter(fw);
				}

				bw.append(values);
				bw.flush();
				bw.close();

				// Esto es para que se regenere la base de datos de MTP,
				// para poder ver el archivo por USB.
				MediaScannerConnection.scanFile(context,
						new String[] { root.getAbsolutePath() }, null, null);
				return FILE_WRITED;
			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
				return FILE_EXCEPTION;
			}
		} else {
			return NO_MEDIA;
		}
	}
}