package ar.edu.unnoba.electronicaDigital.BeaconDuino;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Activity principal que muestra los dispositivos BT disponibles, junto con 
 * intensidad de la señal de cada uno, ordenados por potencia.
 */
public class BeaconListActivity extends ActionBarActivity {

	private static final String TAG = "DeviceListActivity";
	private static final boolean D = true;

	public static final String REQUEST_CODE = "request_code";
	public static final int REQUEST_ENABLE_BT = 3;

	private BluetoothAdapter mBtAdapter;
	private BeaconArrayAdapter mDevicesArrayAdapter;
	private boolean canceledByUser = false;

	/*************************************************************************/
	// Metodos de la activity llamados por android.

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Se obtiene el adaptador Bluetooth.
		mBtAdapter = BluetoothAdapter.getDefaultAdapter();

		// Si el adaptador es nulo, el Bluetooth no es soportado.
		if (mBtAdapter == null) {
			Toast.makeText(this, getString(R.string.no_bluetooth),
					Toast.LENGTH_LONG).show();
			finish();
			return;
		}

		// Seteos iniciales
		setContentView(R.layout.beacon_list_activity);

		getSupportActionBar().setTitle(R.string.app_name);
		setStatus(R.string.scanning);

		mDevicesArrayAdapter = new BeaconArrayAdapter(this,
				R.layout.list_view_item);

		// Busca y carga la lista con los dispositivos recientemente encontrados.
		ListView newDevicesListView = (ListView) findViewById(R.id.discovered_devices);
		newDevicesListView.setAdapter(mDevicesArrayAdapter);
		newDevicesListView.setOnItemClickListener(mDeviceClickListener);

		// Se registra un filtro, para cuando un disposito nuevo es encontrado.
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		this.registerReceiver(mReceiver, filter);

		// Se registra otro filtro cuando finaliza la etapa de 'discovery'.
		filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		this.registerReceiver(mReceiver, filter);
	}

	@Override
	public void onStart() {
		super.onStart();
		// Si el BT no esta encendido, solicita que se active.
		if (!mBtAdapter.isEnabled()) {
			Intent enableIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		} else {
			// Si esta activado, comienza el proceso de busqueda de dispositivos.
			this.doDiscovery();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		canceledByUser = true;
		mBtAdapter.cancelDiscovery();
		mDevicesArrayAdapter.clear();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// Si se cierra la aplicacion, se detiene el proceso de 
		// descubrimiento de dispositivos.
		if (mBtAdapter != null) {
			canceledByUser = true;
			mBtAdapter.cancelDiscovery();
		}

		// Se desregistran los listeners.
		this.unregisterReceiver(mReceiver);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Se agregan items a la 'action bar' si esta presente.
		getMenuInflater().inflate(R.menu.device_list_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Manejo de items seleccionados en la 'action bar'.
		int id = item.getItemId();
		switch (id) {
		case R.id.item_menu_refrescar:
			mBtAdapter.cancelDiscovery();
			return true;
		case R.id.item_menu_read_file:
			launchReadFileActivity();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/*************************************************************************/
	// Metodos de la activity que modifican la interfaz, 
	// pero son llamados por la misma activity.

	private final void setStatus(int resId) {
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setSubtitle(resId);
	}

	/*************************************************************************/
	// Metodos que tienen que ver con bluetooth.

	/**
	 * Comienza la busqueda de dispositivos (BluetoothAdapter).
	 */
	private void doDiscovery() {
		if (D) {
			Log.d(TAG, "doDiscovery() - " + String.valueOf(canceledByUser));
		}

		if (mBtAdapter.isDiscovering()) {
			// Cuando lo detenemos, se limpia la lista y se reinicia la busqueda.
			mBtAdapter.cancelDiscovery();
		} else {
			mBtAdapter.startDiscovery();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_ENABLE_BT:
			if (resultCode == Activity.RESULT_OK) {
				doDiscovery();
			} else {
				finish();
			}
			break;

		default:
			super.onActivityResult(requestCode, resultCode, data);
			break;
		}
	}

	
	/**
	 * Conexion con un dispositivo de la lista.
	 */
	private void connectWithDevice(BeaconObject device) {
		// Se cancela la busqueda, al conectarse a un dispositivo, porque es costoso.
		canceledByUser = true;
		mBtAdapter.cancelDiscovery();
		//Se crea el Intent de la Activity para obtener los datos de un dispositivo.
		Intent intent = new Intent(this, BeaconDuinoDataActivity.class);
		intent.putExtra(BeaconDuinoDataActivity.DEVICE_ADDRESS,
				device.getAddress());
		intent.putExtra(BeaconDuinoDataActivity.DEVICE_NAME, device.getName());
		intent.putExtra(BeaconDuinoDataActivity.DEVICE_SIGNAL_POWER, String.valueOf(device.getSignalPercentage()));
		intent.putExtra(BeaconDuinoDataActivity.DEVICE_RSSI, String.valueOf(device.getRssi()));
		//Se inicializa la Activity nueva.
		startActivity(intent);
		finish();
	}

	
	/**
	 * Evento lanzado al hacer click en el icono para ver el documento generado
	 * con las lecturas realizadas de los almacigos. 
	 */
	private void launchReadFileActivity() {
		canceledByUser = true;
		mBtAdapter.cancelDiscovery();
		Intent intent = new Intent(this, BeaconDuinoReadFileActivity.class);
		startActivity(intent);
		finish();
	}

	/*************************************************************************/
	
	// Listener del click de los dispositivos de la lista ListViews
	private OnItemClickListener mDeviceClickListener = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> av, View v, int position, long id) {
			BeaconObject clickedDevice = mDevicesArrayAdapter.getItem(position);
			connectWithDevice(clickedDevice);
		}
	};

	// BroadcastReceiver escucha los dispositivos descubiertos
	// y cambia el titulo cuando finaliza.
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			// El discovery encuentra un dispositivo.
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				// Obtiene el objeto BluetoothDevice
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				short rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,
						Short.MIN_VALUE);

				// Nuestro objeto bluetooth para mostrarlo en la lista custom.
				// De esta manera, mantenemos todos los datos.
				BeaconObject deviceInfo = new BeaconObject();
				deviceInfo.setName(device.getName());
				deviceInfo.setAddress(device.getAddress());
				deviceInfo.setRssi(rssi);

				mDevicesArrayAdapter.add(deviceInfo);
				// Cuando finaliza el discovery, cambia el titulo de la Activity.
			}
			if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
				if (!canceledByUser) {
					mDevicesArrayAdapter.clear();
					mBtAdapter.startDiscovery();
				} else {
					canceledByUser = false;
				}
			}
		}
	};
}