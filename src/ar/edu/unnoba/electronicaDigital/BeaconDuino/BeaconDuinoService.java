package ar.edu.unnoba.electronicaDigital.BeaconDuino;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * Esta clase se encarga de inicializar y manejar las conexiones Bluetooth
 * con otros dispositivos. Tiene un thread que escucha las conexiones 
 * entrantes, un thread para conectarse con un dispositivo, y otro thread 
 * para realizar la transmision de datos cuando se conecta.
 */
public class BeaconDuinoService {

	private static final String TAG = "BeaconDuinoService";
	private static final boolean D = true;

	private final Handler mHandler;
	private ConnectThread mConnectThread;
	private ConnectedThread mConnectedThread;
	private int mState;

	private boolean stoppedByUser = false;

	// Indicadores del estado actual de la conexion.
	public static final int STATE_NONE = 0; // No esta haciendo nada.
	public static final int STATE_CONNECTING = 2; // Iniciando una conexion saliente.
	public static final int STATE_CONNECTED = 3; // Conectado a un dispositivo remoto.

	/**
	 * Se construye el objeto.
	 */
	public BeaconDuinoService(Context context, Handler handler) {
		mState = STATE_NONE;
		mHandler = handler;
		mHandler.obtainMessage(BeaconDuinoDataActivity.READY).sendToTarget();
	}

	/**
	 * Se setea el estado actual de la conexion.
	 */
	private synchronized void setState(int state) {
		if (D)
			Log.d(TAG, "setState() " + mState + " -> " + state);
		mState = state;

		// Se le pasa el nuevo estado al Handler para que 
		// la Activity pueda actualizar la interfaz.
		mHandler.obtainMessage(BeaconDuinoDataActivity.MESSAGE_STATE_CHANGE,
				state, -1).sendToTarget();
	}

	/**
	 * Se obtiene el estado actual de la conexion.
	 */
	public synchronized int getState() {
		return mState;
	}

	/**
	 * Inicializa el servicio. Comienza el AcceptThread para iniciar una sesion
	 * en modo 'escucha' (server). Es llamado por la Activity onResume()
	 */
	public synchronized void start() {
		if (D)
			Log.d(TAG, "start");

		// Cancela cualquier thread intentando hacer una conexion.
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}

		// Cancela cualquier thread actualmente conectado.
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		setState(STATE_NONE);
	}

	/**
	 * Comienza el ConnectThread para iniciar una conexion a un
	 * dispositivo remoto.
	 */
	public synchronized void connect(BluetoothDevice device) {
		Log.d(TAG, "connect to: " + device);

		// Cancela cualquier thread intentando hacer una conexion.
		if (mState == STATE_CONNECTING) {
			if (mConnectThread != null) {
				mConnectThread.cancel();
				mConnectThread = null;
			}
		}

		// Cancela cualquier thread actualmente conectado.
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		// Inicializa el thread.
		mConnectThread = new ConnectThread(device);
		mConnectThread.start();
		setState(STATE_CONNECTING);
	}

	/**
	 * Inicializa el ConnectedThread para comenzar a manejar
	 * una conexion Bluetooth.
	 */
	public synchronized void connected(BluetoothSocket socket,
			BluetoothDevice device) {
		if (D)
			Log.d(TAG, "connected");

		// Cancela el thread que comenzo la conexion.
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}

		// Cancela cualquier thread actualmente conectado.
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		// Arranca el thread para manejar la conexion y realizar transmisiones.
		mConnectedThread = new ConnectedThread(socket);
		mConnectedThread.start();

		// Envia el nombre del dispositivo conectado a la Activity de la interfaz.
		Message msg = mHandler
				.obtainMessage(BeaconDuinoDataActivity.MESSAGE_DEVICE_NAME);
		Bundle bundle = new Bundle();
		bundle.putString(BeaconDuinoDataActivity.DEVICE_NAME, device.getName());
		msg.setData(bundle);
		mHandler.sendMessage(msg);

		setState(STATE_CONNECTED);
	}

	/**
	 * Detiene todos los threads.
	 */
	public synchronized void stop(boolean stoppedByUser) {
		if (D)
			Log.d(TAG, "stop");

		this.stoppedByUser = stoppedByUser;

		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}

		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		setState(STATE_NONE);
	}

	/**
	 * Escribe en el ConnectedThread de manera asincronica.
	 */
	public void write(byte[] out) {
		ConnectedThread r;

		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;
		}

		r.write(out);
	}

	/**
	 * Indica que el intento de conexion fallo, y notifica a la interfaz.
	 */
	private void connectionFailed() {
		setState(STATE_NONE);
		//Mensaje de error.
		Message msg = mHandler
				.obtainMessage(BeaconDuinoDataActivity.MESSAGE_TOAST);
		Bundle bundle = new Bundle();
		bundle.putInt(BeaconDuinoDataActivity.TOAST, R.string.connecting_error);
		msg.setData(bundle);
		mHandler.sendMessage(msg);
		this.stop(false);
	}

	/**
	 * Indica que se perdio la conexion y notifica.
	 */
	private void connectionLost() {
		this.stop(this.stoppedByUser);
		if (this.stoppedByUser) {
			this.stoppedByUser = false;
		} else {
			setState(STATE_NONE);
			
			Message msg = mHandler
					.obtainMessage(BeaconDuinoDataActivity.MESSAGE_TOAST);
			Bundle bundle = new Bundle();
			bundle.putInt(BeaconDuinoDataActivity.TOAST,
					R.string.lost_connection);
			msg.setData(bundle);
			mHandler.sendMessage(msg);
		}
	}

	/**
	 * Este thread corre mientras se intenta hacer una conexion saliente con un 
	 * dispositivo. Siempre corre, aunque la conexion sea exitosa o falle.
	 */
	private class ConnectThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final BluetoothDevice mmDevice;

		public ConnectThread(BluetoothDevice device) {
			mmDevice = device;
			BluetoothSocket tmp = null;

			// Se obtiene un BluetoothSocket para una conexion con el 
			// dispositivo (BluetoothDevice) dado.
			try {
				Method method;
				method = device.getClass().getMethod("createRfcommSocket",
						new Class[] { int.class });
				tmp = (BluetoothSocket) method.invoke(device, 1);
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
			mmSocket = tmp;
		}

		public void run() {
			Log.i(TAG, "BEGIN mConnectThread SocketType:");
			setName("ConnectThread");

			// Hace una conexion al BluetoothSocket
			try {
				// Llamada bloqueante, regresa luego de una conexion
				// exitosa o una excepcion.
				mmSocket.connect();
			} catch (IOException e) {
				Log.e(TAG, "IOException On Connect Thread: " + e.getMessage());
				connectionFailed();
				try {
					mmSocket.close();
				} catch (IOException e2) {
					Log.e(TAG,
							"unable to close() socket during connection failure",
							e2);
				}
				return;
			}

			synchronized (BeaconDuinoService.this) {
				mConnectThread = null;
			}

			connected(mmSocket, mmDevice);
		}

		public void cancel() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}
		}
	}

	/**
	 * Este thread maneja todas las conexiones entrantes y salientes.
	 */
	private class ConnectedThread extends Thread {
		private final BluetoothSocket mmSocket;
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;

		public ConnectedThread(BluetoothSocket socket) {
			Log.d(TAG, "create ConnectedThread");
			mmSocket = socket;
			InputStream tmpIn = null;
			OutputStream tmpOut = null;

			try {
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			} catch (IOException e) {
				Log.e(TAG, "temp sockets not created", e);
			}

			mmInStream = tmpIn;
			mmOutStream = tmpOut;
		}

		public void run() {
			Log.i(TAG, "BEGIN mConnectedThread");
			byte[] buffer = new byte[1024];
			int bytes;

			while (true) {
				try {
					bytes = mmInStream.read(buffer);
					byte[] copy = new byte[bytes];
					System.arraycopy(buffer, 0, copy, 0, bytes);
					mHandler.obtainMessage(
							BeaconDuinoDataActivity.MESSAGE_READ, bytes, -1,
							copy).sendToTarget();
				} catch (IOException e) {
					Log.d(TAG, "disconnected", e);
					connectionLost();
					break;
				}
			}
		}

		public void write(byte[] buffer) {
			try {
				mmOutStream.write(buffer);

			} catch (IOException e) {
				Log.e(TAG, "Exception during write", e);
			}
		}

		public void cancel() {
			try {
				mmSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}
		}
	}
}