package ar.edu.unnoba.electronicaDigital.BeaconDuino;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity que muestra el archivo .csv generado con las lecturas de los almacigos.
 */
public class BeaconDuinoReadFileActivity extends ActionBarActivity {

	private TextView fileView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.beacon_duino_read_file);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		this.fileView = (TextView) findViewById(R.id.file_view);
	}

	@Override
	protected void onStart() {
		super.onStart();
		String fileContent = BeaconDuinoTools.readDataFile();
		if (fileContent != null) {
			getSupportActionBar().setSubtitle(BeaconDuinoTools.getFileName());
			this.fileView.setText(fileContent);
		} else {
			Toast.makeText(this, getString(R.string.no_file),
					Toast.LENGTH_SHORT).show();
			onBackPressed();
		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, BeaconListActivity.class);
		startActivity(intent);
		finish();
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.read_file_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			onBackPressed();
			return true;
		case R.id.item_menu_enviar_archivo:
			showShareIntent();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void showShareIntent() {
		Intent shareIntent = new Intent();
		shareIntent.setAction(Intent.ACTION_SEND);
		shareIntent.putExtra(Intent.EXTRA_STREAM,
				Uri.fromFile(BeaconDuinoTools.getFile()));
		shareIntent.setType("text/plain");
		startActivity(Intent.createChooser(shareIntent,
				getResources().getText(R.string.share)));
	}
}