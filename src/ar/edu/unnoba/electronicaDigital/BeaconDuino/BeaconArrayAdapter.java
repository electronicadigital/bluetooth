package ar.edu.unnoba.electronicaDigital.BeaconDuino;

import java.util.Comparator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BeaconArrayAdapter extends ArrayAdapter<BeaconObject> {
	private final Context context;
	private final int resource;

	public BeaconArrayAdapter(Context context, int resource) {
		super(context, resource);
		this.context = context;
		this.resource = resource;
	}

	static class ViewHolder {
		public TextView name;
		public ImageView signalPower;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Patron ViewHolder, para aumentar la performance.
		View rowView = convertView;
		// reuse views
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(this.resource, null);
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.name = (TextView) rowView.findViewById(R.id.firstLine);
			viewHolder.signalPower = (ImageView) rowView
					.findViewById(R.id.signal_image);
			rowView.setTag(viewHolder);
		}

		ViewHolder holder = (ViewHolder) rowView.getTag();
		BeaconObject device = this.getItem(position);

		holder.name.setText(device.getName());
		holder.signalPower.setImageResource(getImageResId(device.getSignalPercentage()));
		return rowView;
	}

	private int getImageResId(double porcentaje) {
		// En base al porcentaje, devolvemos el identificador de una u otra imagen.
		if (porcentaje < 20) {
			return R.drawable.s_20;
		} else if (porcentaje < 40) {
			return R.drawable.s_40;
		} else if (porcentaje < 60) {
			return R.drawable.s_60;
		} else if (porcentaje < 80) {
			return R.drawable.s_80;
		} else {
			return R.drawable.s_100;
		}
	}

	// Agregamos ordenando por intensidad de la señal.
	@Override
	public void add(BeaconObject obj) {
		super.add(obj);
		this.sort(new BeaconObjectComparator());
	}

	class BeaconObjectComparator implements Comparator<BeaconObject> {
		@Override
		public int compare(BeaconObject lhs, BeaconObject rhs) {
			if (lhs.getRssi() < rhs.getRssi()) {
				return 1;
			}
			if (lhs.getRssi() > rhs.getRssi()) {
				return -1;
			}
			return 0;
		}

	}
}