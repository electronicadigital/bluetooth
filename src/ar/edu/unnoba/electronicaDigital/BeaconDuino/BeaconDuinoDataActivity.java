package ar.edu.unnoba.electronicaDigital.BeaconDuino;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Actividad que recibe los datos del arduino y los muestra.
 */
public class BeaconDuinoDataActivity extends ActionBarActivity {

	private static final String TAG = "BeaconDuinoData";
	private static final boolean D = true;

	// Tipos de mensaje enviados desde el manejador de BeaconDuinoService.
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;
	public static final int READY = 100;

	public static final String DEVICE_NAME = "device_name";
	public static final String DEVICE_ADDRESS = "device_address";
	public static final String DEVICE_RSSI = "device_rssi";
	public static final String DEVICE_SIGNAL_POWER = "device_signal_power";
	public static final String TOAST = "toast";

	// Caracteres de comunicacion con Arduino
	private static final String RECEIVED_END_STREAM_CHARACTER = "$#endStream#$";
	private static final String SENDED_END_STREAM_CHARACTER = "\n";

	private ListView mDataReceivedView;
	private String mConnectedDeviceName = null;
	private String mConnectedDeviceAddress;
	private String mConnectedDeviceSignalPower;
	private String mConnectedDeviceRssi;
	private ArrayAdapter<String> mDataReceivedArrayAdapter;
	private StringBuffer mOutStringBuffer;
	private BluetoothAdapter mBluetoothAdapter = null;
	private BeaconDuinoService mBeaconDuinoService = null;
	private byte[] bufferB = null;

	/*************************************************************************/
	// Metodos del activity, llamados por Android.

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.beacon_duino_data_activity);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		Bundle extras = getIntent().getExtras();
		this.mConnectedDeviceAddress = extras.getString(DEVICE_ADDRESS);
		this.mConnectedDeviceSignalPower = extras
				.getString(DEVICE_SIGNAL_POWER);
		this.mConnectedDeviceRssi = extras.getString(DEVICE_RSSI);
		getSupportActionBar().setTitle(extras.getString(DEVICE_NAME));
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		setup();
	}

	@Override
	public void onBackPressed() {
		if (mBeaconDuinoService.getState() != BeaconDuinoService.STATE_NONE)
			mBeaconDuinoService.stop(true);
		Intent intent = new Intent(this, BeaconListActivity.class);
		startActivity(intent);
		finish();
		super.onBackPressed();
	}

	@Override
	public void onPause() {
		super.onPause();
		mBeaconDuinoService.stop(true);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mBeaconDuinoService.stop(true);
		if (D)
			Log.e(TAG, "--- ON DESTROY ---");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.device_data_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			onBackPressed();
			return true;
		case R.id.item_menu_reconectar:
			if (mBeaconDuinoService.getState() == BeaconDuinoService.STATE_NONE) {
				this.connectDevice();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	// Metodo disparado por el click del boton RecibirDatos
	// Esta seteado en el XML del layout
	public void buttonReceiveData(View boton) {
		mDataReceivedArrayAdapter.clear();
		receiveData();
	}

	public void buttonSaveData(View boton) {
		if (!mDataReceivedArrayAdapter.isEmpty()) {
			int result = BeaconDuinoTools.saveDataToFile(this,
					mConnectedDeviceName, mConnectedDeviceSignalPower,
					mConnectedDeviceRssi);
			switch (result) {
			case BeaconDuinoTools.FILE_WRITED:
				showToast(R.string.saved_data_ok);
				onBackPressed();
				break;
			case BeaconDuinoTools.FILE_EXCEPTION:
				showToast(R.string.saved_data_fail);
				break;
			case BeaconDuinoTools.NO_MEDIA:
				showToast(R.string.no_media);
				break;
			default:
				break;
			}
		}
	}

	/*************************************************************************/

	// Metodos propios de la Activity, llamadas por ella misma.
	// Modifican la interfaz.

	private void setup() {
		Log.d(TAG, "setup()");

		mDataReceivedArrayAdapter = new ArrayAdapter<String>(this,
				R.layout.received_data_list_item);
		mDataReceivedView = (ListView) findViewById(R.id.received_data);
		mDataReceivedView.setAdapter(mDataReceivedArrayAdapter);

		mOutStringBuffer = new StringBuffer("");

		mBeaconDuinoService = new BeaconDuinoService(this, mHandler);
	}

	private final void setStatus(int resId) {
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setSubtitle(resId);
	}

	private final void showToast(int stringRes) {
		Toast.makeText(this, getString(stringRes), Toast.LENGTH_SHORT).show();
	}

	private final void showToast(int stringRes, String msg) {
		Toast.makeText(this, getString(stringRes, msg), Toast.LENGTH_SHORT)
				.show();
	}

	/*************************************************************************/
	// Metodos de la Activity referidos a la comunicacion por Bluetooth.

	@SuppressLint("HandlerLeak")
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case READY:
				connectDevice();
				break;
			case MESSAGE_STATE_CHANGE:
				if (D)
					Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
				switch (msg.arg1) {
				case BeaconDuinoService.STATE_CONNECTED:
					getSupportActionBar().setTitle(mConnectedDeviceName);
					setStatus(R.string.title_connected);
					mDataReceivedArrayAdapter.clear();
					receiveData();
					break;
				case BeaconDuinoService.STATE_CONNECTING:
					setStatus(R.string.title_connecting);
					break;
				case BeaconDuinoService.STATE_NONE:
					setStatus(R.string.title_not_connected);
					break;
				}
				break;

			case MESSAGE_READ:
				byte[] readBuf = (byte[]) msg.obj;
				if (bufferB != null) {
					bufferB = BeaconDuinoTools.append(bufferB, readBuf);
				} else {
					bufferB = readBuf;
				}

				String temp = new String(bufferB);
				if (temp.endsWith(RECEIVED_END_STREAM_CHARACTER)) {
					bufferB = null;
					temp = temp.replace(RECEIVED_END_STREAM_CHARACTER, "");
					temp = BeaconDuinoTools.processData(temp,
							mConnectedDeviceSignalPower, mConnectedDeviceRssi);
					mDataReceivedArrayAdapter.clear();
					mDataReceivedArrayAdapter.add(temp.trim());
				}
				break;
			case MESSAGE_DEVICE_NAME:
				mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
				showToast(R.string.connected_to, mConnectedDeviceName);
				break;
			case MESSAGE_TOAST:
				showToast(msg.getData().getInt(TOAST));
				break;
			}
		}
	};

	void connectDevice() {
		BluetoothDevice device = mBluetoothAdapter
				.getRemoteDevice(this.mConnectedDeviceAddress);
		Log.d(TAG, "Bt Address: " + String.valueOf(device));
		mBeaconDuinoService.connect(device);
	}

	private void sendMessage(String message) {
		if (mBeaconDuinoService.getState() != BeaconDuinoService.STATE_CONNECTED) {
			showToast(R.string.not_connected);
			return;
		}

		if (message.length() > 0) {
			byte[] send = { 0x0A };
			try {
				send = message.getBytes("US-ASCII");
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
			mBeaconDuinoService.write(send);

			mOutStringBuffer.setLength(0);
		}
	}

	private void receiveData() {
		sendMessage(SENDED_END_STREAM_CHARACTER);
	}
}